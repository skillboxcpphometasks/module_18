#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "StackTemplate.h"


int main()
{
    cout << "Fill in this stack: 1,2,3,4\n";
    Stack<int> intMyStack(4);

    for (int i = 1; i < 5; i++)
    {
        intMyStack.push(i);
    }

    intMyStack.showStack();

    intMyStack.push(44);
    intMyStack.push(40);
    intMyStack.push(30);
    intMyStack.showStack();

    intMyStack.pop();
    intMyStack.pop();
    intMyStack.showStack();

    for (int i = 11; i < 16; i++)
    {
        intMyStack.push(i);
    }
    intMyStack.showStack();

    for (int i = 0; i < 6; i++)
    {
        intMyStack.pop();
    }
    intMyStack.clearEmpty();
    intMyStack.showStack();

    Stack<double> doubleMyStack(3);

    for (int i = 0; i < 3; i++)
    {
        doubleMyStack.push(rand());
    }

    doubleMyStack.showStack();
}
