using namespace std;

template <typename T>
class Stack {
private:
    int size;
    T* arr;
    int last = -1; //index of current stack element

public:
    Stack(int _size);
    ~Stack();
   

    void erase();
    T pop();
    void clearEmpty();
    void push(T value);
    void showStack();
};

template <typename T>
Stack<T>::Stack(int _size) : size(_size)
{
    arr = new T[size];
}

template <typename T>
Stack<T>::~Stack()
{
    delete[] arr;
}

template <typename T>
void Stack<T>::erase()
{
    delete[] arr;

    last = -1;
    size = 0;
}

template <typename T>
T Stack<T>::pop()
{
    return arr[last--];
}

template <typename T>
void Stack<T>::clearEmpty()
{
    size = last + 1;
    T* new_arr = new T[size];
    for (int i = 0; i < size; i++)
    {
        new_arr[i] = arr[i];
    }
    delete[] arr;
    arr = new_arr;
}

template <typename T>
void Stack<T>::push(T value)
{
    if (last + 1 >= size)
    {
        size *= 2;
        T* new_arr = new T[size];
        int i;
        for (i = 0; i <= last; i++)
        {
            new_arr[i] = arr[i];
        }
        new_arr[++last] = value;
        delete[] arr;
        arr = new_arr;
    }
    else
    {
        arr[++last] = value;
    }
}

template <typename T>
void Stack<T>::showStack()
{
    cout << "\n";
    for (int i = last; i >= 0; i--)
    {
        cout << setw(7) << arr[i];
    }
}
